var Persona=function(valores) {
	var nombre;
	var apellido;
	var fechaNacimiento;
	var direccion;

	this.getNombre=function() {
		return nombre;
	};
	this.setNombre=function(valor){
		nombre=valor||"";
	};	
	this.getApellido=function(){
		return apellido;
	}
	this.setApellido=function(valor){
		apellido=valor||"";
	};
	this.getfechaNacimiento=function() {
		return fechaNacimiento;
	};
	this.setfechaNacimiento=function(valor){
		fechaNacimiento=valor||0;
	};
	this.getDireccion=function() {
		return direccion;
	};
	this.setDireccion=function(valor){
		direccion=valor||"";
	};		
	
	this.getNombreCompleto=function() {
		return this.getNombre()+ " " + this.getApellido();
	};

	this.getEdad=function() {
		var hoy=new Date();
		var vector=this.getfechaNacimiento().split("/");
		var fn=new Date(vector[2],vector[1],vector[0]);
		return(hoy.getFullYear()-fn.getFullYear());
	};


	this.persona=function(){
		// nombre=valores.nombre ||"";
		// apellido=valor ||"";
		// fechaNacimiento=valores.fechaNacimiento ||"1/1/2000";
		this.setNombre(valores.nombre);
		this.setApellido(valores.apellido);
		this.setfechaNacimiento(valores.fechaNacimiento);
		this.setDireccion(valores.direccion);
	}		
	this.persona();
}

var Trabajador=function(valores) {
	var Sueldo;
	var Hijos;
	var Empresa;
	var fechaEntrada;

		this.getSueldo=function() {
		return Sueldo;
	};
	this.setSueldo=function(valor){
		// if(typeof(Sueldo)=="undefined"){
		// 	sueldo=v||0;
		// }else if 
		Sueldo=valor||0;
	};

	this.getHijos=function() {
		return Hijos;
	};
	this.setHijos=function(valor){
		Hijos=valor ||0;
	};

	this.getEmpresa=function() {
		return Empresa;
	};
	this.setEmpresa=function(valor){
		Empresa=valor||"";
	};

	this.getfechaEntrada=function() { 
		return fechaEntrada;
	};
	this.setfechaEntrada=function(valor){
		fechaEntrada=valor||0;
	};

	//constructor
	this.Trabajador=function(){
		// Hijos=valores.Hijos ||""
		// Empresa=valor ||"";
		// fechaEntrada=valores.fechaEntrada ||"1/1/2000";
		this.setSueldo(valores.Sueldo);
		this.setHijos(valores.Hijos);
		this.setEmpresa(valores.Empresa);
		this.setfechaEntrada(valores.fechaEntrada);
	}		
	this.Trabajador();				
}
  
var vero=new Trabajador({
	Sueldo:5000,
	Hijos:5,
	Empresa:"house",
	fechaEntrada:"11/11/2018"
})  

console.log(vero.getSueldo());
console.log(vero.getHijos());
console.log(vero.getEmpresa());
console.log(vero.getfechaEntrada());



var jose=new Persona({
	nombre:"jose",
	apellido:"vazquez rodriguez",
	direccion:"mi casa",
	fechaNacimiento:"1/1/2000"
});

console.log(jose.getEdad());
console.log(jose.getNombreCompleto());
console.log(jose.getEdad());

//objetos:

// persona=new Persona();

// var persona={
// 	nombre:"Ramon"
// }

