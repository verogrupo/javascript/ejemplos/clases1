//clase Forma
var Forma=function(padre){
	this.area=null;

	this.getArea=function(){
		return this.area;
	}
	this.setArea=function(valor){
		this.area=valor||0;
	}
	this.constructForma=function(padre){
		this.setArea(padre.area);
	}
	this.constructForma(padre);
}
objeto=new Forma({area:100});

//clase Forma:
var Cuadrado=function(valores){

	this.lado=null;

	this.getLado=function(){
		return this.lado;  
	}
	this.setlado=function(valor){
		this.lado=valor||0;
	}
	this.constructCuadrado=function(valores){
		this.setlado(valores.lado);
		Forma.call(this,valores);
	}
	this.constructCuadrado(valores);
}

var objeto1=new Cuadrado({lado:10,area:200});

console.log(objeto1.getArea());
console.log(objeto1.getLado());