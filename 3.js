

//clase seleccionFutbol:
var seleccionFutbol = function(datos) {
        
//propiedades:
        this.id = null;
        this.Nombre = null;
        this.Apellidos = null;
        this.Edad = null;


//métodos getter y setter:
        this.getid = function() {
            return this.id;
        };
        this.setid = function(valor) {
            this.id = valor || 0;
        };
        this.getNombre = function() {
             return this.Nombre;
        };
        this.setNombre = function(valor) {
             this.Nombre = valor || 0;
        };
        this.getApellidos = function() {
              return this.Apellidos;
        };
        this.setApellidos = function(valor) {
             this.Apellidos = valor || 0;
		};
		this.getEdad = function() {
              return this.Edad;
        };
        this.setEdad = function(valor) {
             this.Edad = valor || 0;
		};
		

//constructor:
		this.constructseleccionFutbol= function(datos){
			this.setid(datos.id);
			this.setNombre(datos.Nombre);
			this.setApellidos(datos.Apellidos);
			this.setEdad(datos.Edad);
		};
			this.constructseleccionFutbol(datos);
}	
		
		var objeto = new seleccionFutbol({
			id:3,
			Nombre:"Marco",
			Apellidos:"ole li",
			Edad:"25",

        });
        console.log(objeto);

//clase Futbolista:
var Futbolista = function(datos) {

	    //propiedades:
        this.Dorsal = null;
        this.Demarcacion = null;

        //métodos getter y setter:
        this.getDorsal = function() {
            return this.Dorsal;
        };
        this.setDorsal = function(valor) {
            this.Dorsal = valor || 0;
        };
        this.getDemarcacion = function(valor) {
        	return this.Demarcacion;
        };
        this.setDemarcacion = function(valor) {
            this.Demarcacion = valor || 0;
		};
		
        //métodos propios otra function:
		this.autorizar=function() {
        	console.log('autorizado')
        };

 //constucctor:
 		this.constructorFutbolista = function(datos){
 			this.setDorsal(datos.Dorsal);
 			this.setDemarcacion(datos.Demarcacion);
 		};
 			this.constructorFutbolista(datos);
 			//heredar de la clase principal "padre"
            seleccionFutbol.call(this,datos);

};			
 		//Futbolista.prototype=new seleccionFutbol({}); ***(sobre carga de escritura en clases, 2 mienbros k se llamen igual)
 		
 		var Futbolista1 = new Futbolista({
 			Dorsal:23,
 			Demarcacion:"Delantero",
 			//añade datos del padre
 			id:2,
			Nombre:"Ber",
			Apellidos:"Zas",
			Edad:"20",
 		});
 		console.log(Futbolista1);
 		console.log(Futbolista1.autorizar);
 		//console.log(Futbolista.__proto__.);***(objeto__proto)